/** \file NitschePeriodicMethod.hpp
 * \ingroup nitsche_method
 * \brief Implementation of Nitsche's method for periodic constrains.
 */

/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#ifndef __NITSCHE_PERIODIC_METHOD_HPP__
#define __NITSCHE_PERIODIC_METHOD_HPP__

/** \brief Periodic Nitsche method
* \ingroup nitsche_method
*/
struct PeriodicNitscheConstrains {

  struct CommonData {

    string volumeElemName;
    Range skinFaces;
    double eps;

    map<EntityHandle,vector<VectorDouble > > localCoordsMap;
    map<EntityHandle,vector<int> > inTetFaceGaussPtsNumber;
    map<EntityHandle,vector<int> > inTetTetGaussPtsNumber;
    map<EntityHandle,double> dIstance;
    vector<VectorDouble > dIsplacements;
    vector<VectorDouble > coordsAtGaussPts;
    vector<VectorDouble > hoCoordsAtGaussPts;
    vector<MatrixDouble > stressJacobian;
    vector<MatrixDouble > sTress;

    struct MultiIndexData {
      int gG;
      int sIde;
      EntityType tYpe;
      ublas::vector<int> iNdices;
      ublas::vector<int> dofOrders;
      VectorDouble shapeFunctions;
      MatrixDouble diffShapeFunctions;
      MultiIndexData(
        int gg,int side,EntityType type
      ):
      gG(gg),
      sIde(side),
      tYpe(type) {
      }
    };

    typedef multi_index_container<
      MultiIndexData,
      indexed_by<
        ordered_non_unique< BOOST_MULTI_INDEX_MEMBER(MultiIndexData,int,MultiIndexData::gG) >,
        ordered_non_unique< BOOST_MULTI_INDEX_MEMBER(MultiIndexData,int,MultiIndexData::sIde) >,
        ordered_non_unique< BOOST_MULTI_INDEX_MEMBER(MultiIndexData,EntityType,MultiIndexData::tYpe) >,
        ordered_unique<
        composite_key<
          MultiIndexData,
          member<MultiIndexData,int,&MultiIndexData::gG>,
          member<MultiIndexData,int,&MultiIndexData::sIde>,
          member<MultiIndexData,EntityType,&MultiIndexData::tYpe>
        > >
      >
    > Container;
    Container facesContainer;
    Container volumesContainer;

  };


  struct OpGetFaceData: FaceElementForcesAndSourcesCore::UserDataOperator {

    CommonData &commonData;
    bool fieldDisp;

    OpGetFaceData(CommonData &common_data,bool field_disp = true):
    FaceElementForcesAndSourcesCore::UserDataOperator("DISPLACEMENT",OPROW),
    commonData(common_data),
    fieldDisp(field_disp) {
    }

    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;

      if(data.getIndices().size()==0) PetscFunctionReturn(0);
      try {
        EntityHandle this_face = getNumeredEntFiniteElementPtr()->getEnt();
        int nb_gauss_pts_on_this_face = data.getN().size1();
        for(int ffgg = 0;ffgg<nb_gauss_pts_on_this_face;ffgg++) {
          int gg = commonData.inTetFaceGaussPtsNumber[this_face][ffgg];
          CommonData::MultiIndexData gauss_pt_data(gg,side,type);
          pair<CommonData::Container::iterator,bool> p;
          p = commonData.facesContainer.insert(gauss_pt_data);
          if(!p.second) {
            SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data not inserted");
          }
          CommonData::MultiIndexData &p_data = const_cast<CommonData::MultiIndexData&>(*p.first);
          VectorDouble &shape_fun = p_data.shapeFunctions;
          int nb_shape_fun = data.getN().size2();
          shape_fun.resize(nb_shape_fun);
          cblas_dcopy(nb_shape_fun,&data.getN()(ffgg,0),1,&shape_fun[0],1);
          p_data.iNdices = data.getIndices();
          p_data.dofOrders.resize(data.getFieldDofs().size(),false);
          for(unsigned int dd = 0;dd<data.getFieldDofs().size();dd++) {
            p_data.dofOrders[dd] = data.getFieldDofs()[dd]->getDofOrder();
          }
          int nb_dofs = data.getFieldData().size();
          if(type == MBVERTEX) {
            commonData.dIsplacements[gg].resize(3,false);
            commonData.coordsAtGaussPts[gg].resize(3,false);
            commonData.hoCoordsAtGaussPts[gg].resize(3,false);
            for(int rr = 0;rr<3;rr++) {
              (commonData.dIsplacements[gg])[rr] = cblas_ddot(
                nb_dofs/3,&data.getFieldData()[rr],3,&data.getN()(ffgg,0),1
              );
              if(!fieldDisp) {
                (commonData.dIsplacements[gg])[rr] -= getHoCoordsAtGaussPts()(ffgg,rr);
              }
              commonData.coordsAtGaussPts[gg][rr] = getCoordsAtGaussPts()(ffgg,rr);
              commonData.hoCoordsAtGaussPts[gg][rr] = getHoCoordsAtGaussPts()(ffgg,rr);
            }
          } else {
            for(int rr = 0;rr<3;rr++) {
              commonData.dIsplacements[gg][rr] += cblas_ddot(
                nb_dofs/3,&data.getFieldData()[rr],3,&data.getN()(ffgg,0),1
              );
            }
          }
        }
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      //cerr << "done\n";
      PetscFunctionReturn(0);
    }

  };

  struct PeriodicFace: FaceElementForcesAndSourcesCore {

    CommonData &commonData;
    PeriodicFace(MoFEM::Interface &m_field,CommonData &common_data):
    FaceElementForcesAndSourcesCore(m_field),
    commonData(common_data) {}

    int getRule(int order) { return -1; }

    PetscErrorCode setGaussPts(int order) {
      PetscFunctionBegin;

      try {

        EntityHandle face = numeredEntFiniteElementPtr->getEnt();
        gaussPts.resize(3,commonData.localCoordsMap[face].size());
        //cerr << commonData.localCoordsMap[face].size() << endl;
        for(unsigned int ffgg = 0;ffgg!=commonData.localCoordsMap[face].size();ffgg++) {
          //cerr << commonData.localCoordsMap[face][ffgg] << endl;
          gaussPts(0,ffgg) = commonData.localCoordsMap[face][ffgg][0];
          gaussPts(1,ffgg) = commonData.localCoordsMap[face][ffgg][1];
          gaussPts(2,ffgg) = 0; // not used, since filed on this face is not integrated
        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }

  };

  struct OpGetVolumeData: VolumeElementForcesAndSourcesCore::UserDataOperator {

    NonlinearElasticElement::CommonData &commonData;
    CommonData &periodicCommonData;

    OpGetVolumeData(
      NonlinearElasticElement::CommonData &common_data,
      CommonData &periodic_common_data
    ):
    VolumeElementForcesAndSourcesCore::UserDataOperator("DISPLACEMENT",OPROW),
    commonData(common_data),
    periodicCommonData(periodic_common_data) {
    }

    PetscErrorCode doWork(int side,EntityType type,DataForcesAndSurcesCore::EntData &data) {
      PetscFunctionBegin;
      if(data.getIndices().size()==0) PetscFunctionReturn(0);
      try {
        EntityHandle this_tet = getNumeredEntFiniteElementPtr()->getEnt();
        int nb_gauss_pts = data.getN().size1();
        for(int ffgg = 0;ffgg<nb_gauss_pts;ffgg++) {
          int gg = periodicCommonData.inTetTetGaussPtsNumber[this_tet][ffgg];
          CommonData::MultiIndexData gauss_pt_data(gg,side,type);
          pair<CommonData::Container::iterator,bool> p;
          p = periodicCommonData.volumesContainer.insert(gauss_pt_data);
          if(!p.second) {
            SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data not inserted");
          }
          CommonData::MultiIndexData &p_data = const_cast<CommonData::MultiIndexData&>(*p.first);
          MatrixDouble &diff_shape_fun = p_data.diffShapeFunctions;
          diff_shape_fun = data.getDiffN(ffgg);
          p_data.iNdices = data.getIndices();
          if(type == MBVERTEX) {
            periodicCommonData.stressJacobian[gg] = commonData.jacStress[ffgg];
            periodicCommonData.sTress[gg] = commonData.sTress[ffgg];
          }
        }
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }

  };

  struct PeriodicVolume: VolumeElementForcesAndSourcesCore {

    EntityHandle forFace;
    CommonData &commonData;

    PeriodicVolume(MoFEM::Interface &m_field,CommonData &common_data):
    VolumeElementForcesAndSourcesCore(m_field),
    forFace(0),
    commonData(common_data) {}
    int getRule(int order) { return -1; }
    PetscErrorCode setGaussPts(int order) {

      PetscFunctionBegin;
      try {
        gaussPts.resize(4,0,false);
        EntityHandle tet = numeredEntFiniteElementPtr->getEnt();
        int vffgg = 0;
        for(int ff = 0;ff<4;ff++) {
          EntityHandle face;
          rval = mField.get_moab().side_element(tet,2,ff,face); CHKERRQ_MOAB(rval);
          if(face!=forFace) continue;
          if(commonData.localCoordsMap.find(face)!=commonData.localCoordsMap.end()) {
            const double coords_tet[12] = { 0,0,0, 1,0,0, 0,1,0, 0,0,1 };
            int nb_face_gauss_pts = commonData.localCoordsMap[face].size();
            gaussPts.resize(4,nb_face_gauss_pts);
            commonData.inTetTetGaussPtsNumber[tet].resize(nb_face_gauss_pts);
            for(int ffgg = 0;ffgg!=nb_face_gauss_pts;ffgg++,vffgg++) {
              commonData.inTetTetGaussPtsNumber[tet][vffgg] = commonData.inTetFaceGaussPtsNumber[face][ffgg];
              double N[3];
              VectorDouble &local_coords = commonData.localCoordsMap[face][ffgg];
              ierr = ShapeMBTRI(N,&local_coords[0],&local_coords[1],1); CHKERRQ(ierr);
              for(int dd = 0;dd<3;dd++) {
                gaussPts(dd,vffgg) =
                N[0]*coords_tet[3*dataH1.facesNodes(ff,0)+dd]+
                N[1]*coords_tet[3*dataH1.facesNodes(ff,1)+dd]+
                N[2]*coords_tet[3*dataH1.facesNodes(ff,2)+dd];
              }
              gaussPts(3,vffgg) = 0;
            }
          } else {
            SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"data inconstancy");
          }
        }
        //cerr << gaussPts << endl;
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }
      PetscFunctionReturn(0);
    }

  };

  struct MyNitscheVolume: public NitscheMethod::MyVolumeFE {

    CommonData &periodicCommonData;
    PeriodicFace periodicFace;
    PeriodicVolume periodicVolume;

    bool iNit;
    AdaptiveKDTree tRee;

    MyNitscheVolume(
      MoFEM::Interface &m_field,
      NitscheMethod::BlockData &block_data,
      NitscheMethod::CommonData &common_data,
      CommonData &periodic_common_data
    ):
    NitscheMethod::MyVolumeFE(m_field,block_data,common_data),
    periodicCommonData(periodic_common_data),
    periodicFace(m_field,periodic_common_data),
    periodicVolume(m_field,periodic_common_data),
    iNit(false),
    tRee(&m_field.get_moab()) {
      periodicFace.getOpPtrVector().push_back(
        new OpGetFaceData(periodic_common_data)
      );
    }

    EntityHandle treeRootSet;
    PetscErrorCode iNitialise() {
      PetscFunctionBegin;

      treeRootSet = 0;
      rval = tRee.build_tree(periodicCommonData.skinFaces,&treeRootSet); CHKERRQ_MOAB(rval);
      //cerr << blockData.fAces << endl;
      PetscFunctionReturn(0);
    }

    PetscErrorCode doAdditionalJobWhenGuassPtsAreCalulated() {
      PetscFunctionBegin;




      if(!iNit) {
        ierr = iNitialise(); CHKERRQ(ierr);
        iNit = true;
      }

      //cerr << "part1\n";
      try {
        const double tol = 1e-18;
        periodicCommonData.facesContainer.clear();
        periodicCommonData.volumesContainer.clear();
        periodicCommonData.localCoordsMap.clear();
        periodicCommonData.dIstance.clear();
        periodicCommonData.inTetFaceGaussPtsNumber.clear();
        periodicCommonData.inTetTetGaussPtsNumber.clear();
        periodicCommonData.dIsplacements.clear();
        periodicCommonData.stressJacobian.clear();
        periodicCommonData.sTress.clear();
        periodicCommonData.coordsAtGaussPts.clear();
        periodicCommonData.hoCoordsAtGaussPts.clear();
        int gg = 0;
        for(int ff = 0;ff<4;ff++) {
          if(commonData.facesFePtr[ff]==NULL) continue;
          for(unsigned int fgg = 0;fgg<commonData.faceGaussPts[ff].size2();fgg++,gg++) {
            VectorDouble& ray = commonData.rAy[ff];
            VectorAdaptor coords = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(
                3,&commonData.hoCoordsAtGaussPts[ff](fgg,0)
              )
            );
            vector<EntityHandle> triangles_out;
            vector<double> distance_out;
            rval = tRee.ray_intersect_triangles(
              treeRootSet,tol,&ray[0],&coords[0],triangles_out,distance_out
            ); CHKERRQ_MOAB(rval);
            double diffNTRI[6];
            ierr = ShapeDiffMBTRI(diffNTRI); CHKERRQ(ierr);
            vector<EntityHandle> triangles_out_on_other_side;
            vector<double> distance_out_on_other_side;
            {
              vector<EntityHandle>::iterator hit = triangles_out.begin();
              vector<double>::iterator dit = distance_out.begin();
              for(;dit!=distance_out.end();dit++,hit++) {
                const EntityHandle* conn;
                int number_nodes;
                rval = mField.get_moab().get_connectivity(*hit,conn,number_nodes,true); CHKERRQ_MOAB(rval);
                double coords[9];
                rval = mField.get_moab().get_coords(conn,number_nodes,coords); CHKERRQ_MOAB(rval);
                double normal[3];
                ierr = ShapeFaceNormalMBTRI(diffNTRI,coords,normal); CHKERRQ(ierr);
                *dit *= cblas_ddot(3,normal,1,&ray[0],1)/cblas_dnrm2(3,normal,1);
                if(*dit>tol) {
                  triangles_out_on_other_side.push_back(*hit);
                  distance_out_on_other_side.push_back(*dit);
                }
              }
            }
            vector<double>::iterator max_dit = max_element(
              distance_out_on_other_side.begin(),distance_out_on_other_side.end()
            );
            EntityHandle other_face = triangles_out_on_other_side[
              std::distance(distance_out_on_other_side.begin(),max_dit)
            ];
            VectorDouble other_coords = coords+ray*(*max_dit);
            periodicCommonData.dIstance[other_face] = *max_dit;
            VectorDouble local_coords;
            local_coords.resize(2);
            {
              const EntityHandle* conn;
              int number_nodes;
              rval = mField.get_moab().get_connectivity(other_face,conn,number_nodes,false); CHKERRQ_MOAB(rval);

              {
                double face_coords[18];
                rval = mField.get_moab().get_coords(conn,number_nodes,face_coords); CHKERRQ_MOAB(rval);

                VectorDouble res,d_local_coords;
                res.resize(3,false);
                d_local_coords.resize(2,false);

                double N[number_nodes];
                double diffNTRI[number_nodes*2];
                double nrm2,nrm2_res;
                const double eps = 1e-14;
                const int max_it = 10;

                int it = 0;
                local_coords.clear();
                do {
                  if(number_nodes == 3) {
                    ierr = ShapeMBTRI(N,&local_coords[0],&local_coords[1],1); CHKERRQ(ierr);
                  } else if(number_nodes == 6) {
                    ierr = ShapeMBTRIQ(N,&local_coords[0],&local_coords[1],1); CHKERRQ(ierr);
                  } else {
                    SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_IMPLEMENTED,"not implemented");
                  }
                  res.clear();
                  for(int dd = 0;dd<3;dd++) {
                    res[dd] += cblas_ddot(number_nodes,N,1,&face_coords[dd],3)-other_coords[dd];
                  }
                  nrm2_res = cblas_dnrm2(3,&res[0],1);
                  if(number_nodes == 3) {
                    ierr = ShapeDiffMBTRI(diffNTRI); CHKERRQ(ierr);
                  } else {
                    ierr = ShapeDiffMBTRIQ(diffNTRI,&local_coords[0],&local_coords[1],1); CHKERRQ(ierr);
                  }
                  MatrixDouble A;
                  A.resize(3,2);
                  for(int dd = 0;dd<3;dd++) {
                    A(dd,0) = cblas_ddot(number_nodes,&diffNTRI[0],2,&face_coords[dd],3);
                    A(dd,1) = cblas_ddot(number_nodes,&diffNTRI[1],2,&face_coords[dd],3);
                  }
                  MatrixDouble ATA;
                  ATA = prod(trans(A),A);
                  VectorDouble ATres;
                  ATres = prod(trans(A),res);
                  double det = (ATA(0,0)*ATA(1,1)-ATA(0,1)*ATA(1,0));
                  d_local_coords[0] = -(ATA(1,1)*ATres[0]-ATA(0,1)*ATres[1])/det;
                  d_local_coords[1] = -(ATA(0,0)*ATres[1]-ATA(1,0)*ATres[0])/det;
                  local_coords += d_local_coords;
                  nrm2 = cblas_dnrm2(2,&d_local_coords[0],1);
                  if((it++)>max_it) {
                    cerr << "Warrning: max_it reached in PeriodicNitscheConstrains:: ... ::doAdditionalJobWhenGuassPtsAreCalulated\n";
                    cerr << "nrm2 " << nrm2 << " nrm2_res " << nrm2_res << endl;
                    cerr << "res " << res << endl;
                    cerr << "other_coords " << other_coords << endl;
                    break;
                  }
                } while(nrm2>eps || nrm2_res>eps);
                if(local_coords[0]<-eps) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"local_coords[0]=%6.4e<eps",local_coords[0]);
                }
                if(local_coords[1]<-eps) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"local_coords[1]=%6.4e<eps",local_coords[1]);
                }
                if(local_coords[0]>1+eps) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"local_coords[0]=%6.4e>1+eps",local_coords[0]);
                }
                if(local_coords[1]>1+eps) {
                  SETERRQ1(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"local_coords[1]=%6.4e>1+eps",local_coords[1]);
                }
                if(local_coords[0]+local_coords[1]>1+eps) {
                  SETERRQ1(
                    PETSC_COMM_SELF,
                    MOFEM_DATA_INCONSISTENCY,
                    "local_coords[0]+local_coords[1]>1",
                    local_coords[0]+local_coords[1]
                  );
                }
              }

            }
            periodicCommonData.localCoordsMap[other_face].push_back(local_coords);
            periodicCommonData.inTetFaceGaussPtsNumber[other_face].push_back(gg);
          }
        }
        periodicCommonData.dIsplacements.resize(gg);
        periodicCommonData.stressJacobian.resize(gg);
        periodicCommonData.sTress.resize(gg);
        periodicCommonData.coordsAtGaussPts.resize(gg);
        periodicCommonData.hoCoordsAtGaussPts.resize(gg);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      try {
        for(
          map<EntityHandle,vector<int> >::iterator mit = periodicCommonData.inTetFaceGaussPtsNumber.begin();
          mit!=periodicCommonData.inTetFaceGaussPtsNumber.end();
          mit++
        ) {
            //cerr << mit->first << " " << mit->second.size() << endl;
            NumeredEntFiniteElement_multiIndex::index<Composite_Name_And_Ent_mi_tag>::type::iterator it;
            it = returnNumeredEntFiniteElement_multiIndex(
                     problemPtr->numeredFiniteElements)
                     .get<Composite_Name_And_Ent_mi_tag>()
                     .find(
                         boost::make_tuple(blockData.faceElemName, mit->first));
            if (it == returnNumeredEntFiniteElement_multiIndex(
                          problemPtr->numeredFiniteElements)
                          .get<Composite_Name_And_Ent_mi_tag>()
                          .end()) {
              SETERRQ1(
                PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"No finite element found < %s >",
                blockData.faceElemName.c_str()
              );
            }
            //cerr << *it << endl;
            boost::shared_ptr<const NumeredEntFiniteElement> face_ptr = *it;
            periodicFace.copyBasicMethod(*this);
            periodicFace.feName = blockData.faceElemName;
            periodicFace.nInTheLoop = 0;
            periodicFace.numeredEntFiniteElementPtr = face_ptr;
            periodicFace.dataPtr = face_ptr->sPtr->data_dofs;
            periodicFace.rowPtr = face_ptr->rows_dofs;
            periodicFace.colPtr = face_ptr->cols_dofs;
            ierr = periodicFace(); CHKERRQ(ierr);

            Range tets;
            ierr = mField.get_adjacencies_equality(mit->first,3,tets); CHKERRQ(ierr);
            if(tets.size()!=1) {
              SETERRQ1(
                PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"Expected one tet, but is %d",
                tets.size()
              );
            }
            it = returnNumeredEntFiniteElement_multiIndex(
                     problemPtr->numeredFiniteElements)
                     .get<Composite_Name_And_Ent_mi_tag>()
                     .find(boost::make_tuple(periodicCommonData.volumeElemName,
                                             tets[0]));
            if (it == returnNumeredEntFiniteElement_multiIndex(
                          problemPtr->numeredFiniteElements)
                          .get<Composite_Name_And_Ent_mi_tag>()
                          .end()) {
              SETERRQ1(
                PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"No finite element found < %s >",
                periodicCommonData.volumeElemName.c_str()
              );
            }
            boost::shared_ptr<const NumeredEntFiniteElement> tet_ptr = *it;
            periodicVolume.copyBasicMethod(*this);
            periodicVolume.feName = periodicCommonData.volumeElemName;
            periodicVolume.nInTheLoop = 0;
            periodicVolume.numeredEntFiniteElementPtr = tet_ptr;
            periodicVolume.dataPtr = face_ptr->sPtr->data_dofs;
            periodicVolume.rowPtr = face_ptr->rows_dofs;
            periodicVolume.colPtr = face_ptr->cols_dofs;
            periodicVolume.forFace = mit->first;
            ierr = periodicVolume(); CHKERRQ(ierr);

        }
      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  struct OpRhsPeriodicNormal: public NitscheMethod::OpCommon {

    CommonData &periodicCommonData;
    vector<Vec> &F;

    OpRhsPeriodicNormal(
      string field_name,
      NitscheMethod::BlockData &nitsche_block_data,
      NitscheMethod::CommonData &nitsche_common_data,
      NonlinearElasticElement::BlockData &data,
      NonlinearElasticElement::CommonData &common_data,
      CommonData &periodic_common_data,
      vector<Vec> &f,
      bool field_disp = true
    ):
    OpCommon(
      field_name,
      nitsche_block_data,
      nitsche_common_data,
      data,
      common_data,
      field_disp,
      UserDataOperator::OPROW
    ),
    periodicCommonData(periodic_common_data),
    F(f) {
    }

    VectorDouble nF0[6],nF1[6];


    VectorDouble voightStrain;
    VectorDouble dispOnThisSide;
    VectorDouble dispOnOtherSide;

    MatrixDouble matD0,matD1;
    static PetscErrorCode calcualteDMatrix(VectorAdaptor coords,MatrixDouble &mat_d) {
      PetscFunctionBegin;
      mat_d.resize(3,6,false);
      mat_d.clear();
      mat_d(0,0) = coords[0]; mat_d(0,3) = coords[1]*0.5; mat_d(0,5) = coords[2]*0.5;
      mat_d(1,1) = coords[1]; mat_d(1,3) = coords[0]*0.5; mat_d(1,4) = coords[2]*0.5;
      mat_d(2,2) = coords[2]; mat_d(2,4) = coords[1]*0.5; mat_d(2,5) = coords[0]*0.5;
      PetscFunctionReturn(0);
    }

    PetscErrorCode doWork(
      int row_side,EntityType row_type,DataForcesAndSurcesCore::EntData &row_data
    ) {
      PetscFunctionBegin;

      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
      int nb_dofs = row_data.getIndices().size();
      const double gamma = nitscheBlockData.gamma;
      const double phi = nitscheBlockData.phi;
      double eps = periodicCommonData.eps;

      try {

        int gg = 0;
        for(int ff = 0;ff<4;ff++) {
          if(nitscheCommonData.facesFePtr[ff]==NULL) continue;
          int nb_face_gauss_pts = nitscheCommonData.faceGaussPts[ff].size2();
          ierr = getFaceRadius(ff); CHKERRQ(ierr);
          for(int ii = 0;ii<6;ii++) {
            nF0[ii].resize(nb_dofs,false);
            nF0[ii].clear();
            nF1[ii].resize(nb_dofs,false);
            nF1[ii].clear();
          }
          for(int fgg = 0;fgg<nb_face_gauss_pts;fgg++,gg++) {
            ierr = getGammaH(gamma,gg); CHKERRQ(ierr);
            double val = getGaussPts()(3,gg);
            ierr = getJac(row_data,gg,jAc_row); CHKERRQ(ierr);
            ierr = getTractionVariance(gg,fgg,ff,jAc_row,tRac_v); CHKERRQ(ierr);
            VectorAdaptor normal = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(
                3,&nitscheCommonData.faceNormals[ff](fgg,0)
              )
            );
            double area = cblas_dnrm2(3,&normal[0],1);

            voightStrain.resize(6,false);
            dispOnThisSide.resize(3,false);
            dispOnOtherSide.resize(3,false);
            VectorAdaptor x = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(3,&getHoCoordsAtGaussPts()(gg,0))
            );
            ierr = calcualteDMatrix(x,matD0);
            VectorDouble other_x = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(3,&periodicCommonData.hoCoordsAtGaussPts[gg][0])
            );
            ierr = calcualteDMatrix(other_x,matD1);
            const double err = 1e-10;
            double delta = inner_prod(other_x,normal)+inner_prod(x,normal);
            if(fabs(delta)>err) {
              cerr << x << endl;
              cerr << other_x << endl;
              SETERRQ1(
                PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"err = %6.4e",delta
              );
            }

            for(int ii = 0;ii<6;ii++) {
              voightStrain.clear();
              voightStrain[ii] = 1;
              noalias(dispOnThisSide) = prod(matD0,voightStrain);
              noalias(dispOnOtherSide) = prod(matD1,voightStrain);
              for(int dd1 = 0;dd1<nb_dofs/3;dd1++) {
                double n_val = row_data.getN(gg)[dd1];
                for(int dd2 = 0;dd2<3;dd2++) {
                  nF0[ii][3*dd1+dd2] -= dispOnThisSide[dd2]*n_val*val*area;
                  nF0[ii][3*dd1+dd2] += (1-eps)*dispOnOtherSide[dd2]*n_val*val*area;
                }
              }
              for(int dd = 0;dd<nb_dofs;dd++) {
                double dot;
                dot = cblas_ddot(
                  3,&dispOnThisSide[0],1,&tRac_v(0,dd),tRac_v.size2()
                );
                dot -= (1-eps)*cblas_ddot(
                  3,&dispOnOtherSide[0],1,&tRac_v(0,dd),tRac_v.size2()
                );
                nF1[ii][dd] += val*phi*dot;
              }
            }
          }

          for(int ii = 0;ii<6;ii++) {
            nF0[ii] *= -(1/gammaH);
            ierr = VecSetValues(
              F[ii],
              nb_dofs,
              &row_data.getIndices()[0],
              &nF0[ii][0],
              ADD_VALUES
            ); CHKERRQ(ierr);
            nF1[ii] *= -1;
            ierr = VecSetValues(
              F[ii],
              nb_dofs,
              &row_data.getIndices()[0],
              &nF1[ii][0],
              ADD_VALUES
            ); CHKERRQ(ierr);
          }

        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  struct OpLhsPeriodicNormal: public NitscheMethod::OpCommon {

    CommonData &periodicCommonData;

    OpLhsPeriodicNormal(
      const string field_name,
      NitscheMethod::BlockData &nitsche_block_data,
      NitscheMethod::CommonData &nitsche_common_data,
      NonlinearElasticElement::BlockData &data,
      NonlinearElasticElement::CommonData &common_data,
      CommonData &periodic_common_data,
      bool field_disp = true
    ):
    OpCommon(
      field_name,
      nitsche_block_data,
      nitsche_common_data,
      data,
      common_data,
      field_disp,
      UserDataOperator::OPROW
    ),
    periodicCommonData(periodic_common_data) {
    }

    MatrixDouble kMatrix0,kMatrix1;
    MatrixDouble kMatrixOff;

    PetscErrorCode doWork(
      int row_side,EntityType row_type,DataForcesAndSurcesCore::EntData &row_data
    ) {
      PetscFunctionBegin;


      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }
      if(row_data.getIndices().size()==0) PetscFunctionReturn(0);
      int nb_dofs_row = row_data.getIndices().size();
      const double gamma = nitscheBlockData.gamma;
      const double phi = nitscheBlockData.phi;
      double eps = periodicCommonData.eps;

      try {

        int gg = 0;
        for(int ff = 0;ff<4;ff++) {
          if(nitscheCommonData.facesFePtr[ff]==NULL) continue;
          int nb_face_gauss_pts = nitscheCommonData.faceGaussPts[ff].size2();
          vector<vector<MatrixDouble > > &P = nitscheCommonData.P;
          P.resize(4);
          P[ff].resize(nb_face_gauss_pts);
          for(int fgg = 0;fgg<nb_face_gauss_pts;fgg++) {
            P[ff][fgg].resize(3,3);
            P[ff][fgg].clear();
            for(int dd = 0;dd<3;dd++) {
              P[ff][fgg](dd,dd) = 1;
            }
          }
          ierr = getFaceRadius(ff); CHKERRQ(ierr);
          for(int fgg = 0;fgg<nb_face_gauss_pts;fgg++,gg++) {
            ierr = getGammaH(gamma,gg); CHKERRQ(ierr);
            double val = getGaussPts()(3,gg);
            ierr = getJac(row_data,gg,jAc_row); CHKERRQ(ierr);
            ierr = getTractionVariance(gg,fgg,ff,jAc_row,tRac_v); CHKERRQ(ierr);
            VectorAdaptor normal = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(3,&nitscheCommonData.faceNormals[ff](fgg,0))
            );
            double area = cblas_dnrm2(3,&normal[0],1);
            const double alpha = 0.5; // mortar parameter

            CommonData::Container::nth_index<0>::type::iterator it,hi_it;
            it = periodicCommonData.facesContainer.get<0>().lower_bound(gg);
            hi_it = periodicCommonData.facesContainer.get<0>().upper_bound(gg);
            for(;it!=hi_it;it++) {

              const ublas::vector<int> &indices = it->iNdices;
              const VectorDouble &shape = it->shapeFunctions;

              int nb_dofs_col = indices.size();
              if(indices.size()==0) {
                continue;
              }
              kMatrix0.resize(nb_dofs_row,nb_dofs_col,false);
              kMatrix0.clear();
              for(int dd1 = 0;dd1<nb_dofs_row/3;dd1++) {
                double n_row = row_data.getN()(gg,dd1);
                for(int dd2 = 0;dd2<nb_dofs_col/3;dd2++) {
                  double n_col = shape[dd2];
                  for(int dd3 = 0;dd3<3;dd3++) {
                    kMatrix0(3*dd1+dd3,3*dd2+dd3) -= (1-eps)*val*n_row*n_col*area;
                  }
                }
              }
              kMatrix1.resize(nb_dofs_row,nb_dofs_col,false);
              kMatrix1.clear();
              for(int dd1 = 0;dd1<nb_dofs_row;dd1++) {
                for(int dd2 = 0;dd2<nb_dofs_col/3;dd2++) {
                  double n_col = shape[dd2];
                  for(int dd3 = 0;dd3<3;dd3++) {
                    kMatrix1(dd1,3*dd2+dd3) += (1-eps)*phi*val*tRac_v(dd3,dd1)*n_col;
                  }
                }
              }

              kMatrix0 /= gammaH;
              ierr = MatSetValues(
                getFEMethod()->snes_B,
                nb_dofs_row,
                &row_data.getIndices()[0],
                nb_dofs_col,
                &indices[0],
                &kMatrix0(0,0),
                ADD_VALUES
              ); CHKERRQ(ierr);

              ierr = MatSetValues(
                getFEMethod()->snes_B,
                nb_dofs_row,
                &row_data.getIndices()[0],
                nb_dofs_col,
                &indices[0],
                &kMatrix1(0,0),
                ADD_VALUES
              ); CHKERRQ(ierr);

              kMatrix1.resize(nb_dofs_col,nb_dofs_row);
              kMatrix1.clear();
              for(int dd1 = 0;dd1<nb_dofs_col/3;dd1++) {
                double n_col = shape[dd1];
                for(int dd2 = 0;dd2<3;dd2++) {
                  for(int dd3 = 0;dd3<nb_dofs_row;dd3++) {
                    kMatrix1(3*dd1+dd2,dd3) += (1-eps)*val*n_col*tRac_v(dd2,dd3);
                  }
                }
              }

              kMatrix1 *= alpha;
              ierr = MatSetValues(
                getFEMethod()->snes_B,
                nb_dofs_col,
                &indices[0],
                nb_dofs_row,
                &row_data.getIndices()[0],
                &kMatrix1(0,0),
                ADD_VALUES
              ); CHKERRQ(ierr);

            }

            MatrixDouble jac;
            MatrixDouble trac;

            it = periodicCommonData.volumesContainer.get<0>().lower_bound(gg);
            hi_it = periodicCommonData.volumesContainer.get<0>().upper_bound(gg);
            for(;it!=hi_it;it++) {

              int nb = it->iNdices.size();
              jac.resize(9,nb,false);
              jac.clear();
              const MatrixDouble diff_n = it->diffShapeFunctions;
              const MatrixDouble jac_stress = periodicCommonData.stressJacobian[gg];
              for(int dd = 0;dd<nb/3;dd++) {
                for(int rr = 0;rr<3;rr++) {
                  for(int ii = 0;ii<9;ii++) {
                    for(int jj = 0;jj<3;jj++) {
                      jac(ii,3*dd+rr) += jac_stress(ii,3*rr+jj)*diff_n(dd,jj);
                    }
                  }
                }
              }
              trac.resize(3,jac.size2());
              trac.clear();
              for(unsigned int dd2 = 0;dd2<jac.size2();dd2++) {
                for(int nn = 0;nn<3;nn++) {
                  trac(nn,dd2) = -cblas_ddot(3,&jac(3*nn,dd2),jac.size2(),&normal[0],1);
                }
              }

              kMatrixOff.resize(nb_dofs_row,nb);
              kMatrixOff.clear();
              for(int dd1 = 0;dd1<nb_dofs_row/3;dd1++) {
                double n_row = row_data.getN()(gg,dd1);
                for(int dd2 = 0;dd2<3;dd2++) {
                  for(int dd3 = 0;dd3<nb;dd3++) {
                    kMatrixOff(3*dd1+dd2,dd3) += (1-eps)*val*n_row*trac(dd2,dd3);
                  }
                }
              }

              kMatrixOff *= (1-alpha);
              ierr = MatSetValues(
                getFEMethod()->snes_B,
                nb_dofs_row,
                &row_data.getIndices()[0],
                nb,
                &it->iNdices[0],
                &kMatrixOff(0,0),
                ADD_VALUES
              ); CHKERRQ(ierr);

            }

          }
        }

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  struct OpCalculateHomogenisedStressVolumeIntegral: public NonlinearElasticElement::OpRhsPiolaKirchhoff {

    Vec stressHomoGhostVector;

    OpCalculateHomogenisedStressVolumeIntegral(
      const string field_name,
      NonlinearElasticElement::BlockData &data,
      NonlinearElasticElement::CommonData &common_data,
      Vec stress_homo_ghost_vector
    ):
    NonlinearElasticElement::OpRhsPiolaKirchhoff(
      field_name,data,common_data
    ),
    stressHomoGhostVector(stress_homo_ghost_vector) {
    }

    VectorDouble homoStress;

    PetscErrorCode doWork(
      int row_side,EntityType row_type,DataForcesAndSurcesCore::EntData &row_data
    ) {
      PetscFunctionBegin;



      if(row_type != MBVERTEX) PetscFunctionReturn(0);
      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      try {

        homoStress.resize(6,false);
        homoStress.clear();

        for(unsigned int gg = 0;gg<row_data.getN().size1();gg++) {
          const MatrixDouble& stress = commonData.sTress[gg];
          double val = getGaussPts()(3,gg)*getVolume();
          if(getHoGaussPtsDetJac().size()>0) {
            val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
          }
          homoStress[0] += stress(0,0)*val;
          homoStress[1] += stress(1,1)*val;
          homoStress[2] += stress(2,2)*val;
          homoStress[3] += (stress(0,1)+stress(1,0))*0.5*val;
          homoStress[4] += (stress(1,2)+stress(2,1))*0.5*val;
          homoStress[5] += (stress(0,2)+stress(2,0))*0.5*val;
        }

        const int indices_6[6] = {0, 1, 2, 3, 4, 5};
        ierr = VecSetValues(
          stressHomoGhostVector,6,indices_6,&homoStress[0],ADD_VALUES
        ); CHKERRQ(ierr);

      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };

  struct OpCalculateHomogenisedStressSurfaceIntegral: public NitscheMethod::OpCommon {

    Vec stressHomoGhostVector;

    OpCalculateHomogenisedStressSurfaceIntegral(
      const string field_name,
      NitscheMethod::BlockData &nitsche_block_data,
      NitscheMethod::CommonData &nitsche_common_data,
      NonlinearElasticElement::BlockData &data,
      NonlinearElasticElement::CommonData &common_data,
      Vec stress_homo_ghost_vector,
      bool field_disp = 0
    ):
    NitscheMethod::OpCommon(
      field_name,
      nitsche_block_data,
      nitsche_common_data,
      data,
      common_data,
      field_disp,
      UserDataOperator::OPROW
    ),
    stressHomoGhostVector(stress_homo_ghost_vector) {
    }

    MatrixDouble matD;
    VectorDouble tRaction;
    VectorDouble homoStress;

    VectorDouble resDisp;
    VectorDouble dispOnOtherSide;

    PetscErrorCode doWork(
      int row_side,EntityType row_type,DataForcesAndSurcesCore::EntData &row_data
    ) {
      PetscFunctionBegin;



      if(row_type != MBVERTEX) PetscFunctionReturn(0);
      if(dAta.tEts.find(getNumeredEntFiniteElementPtr()->getEnt()) == dAta.tEts.end()) {
        PetscFunctionReturn(0);
      }

      try {

        homoStress.resize(6,false);
        homoStress.clear();

        int gg = 0;
        for(int ff = 0;ff<4;ff++) {
          if(nitscheCommonData.facesFePtr[ff]==NULL) continue;
          int nb_face_gauss_pts = nitscheCommonData.faceGaussPts[ff].size2();
          for(int fgg = 0;fgg<nb_face_gauss_pts;fgg++,gg++) {
            double val = getGaussPts()(3,gg);
            const MatrixDouble& stress = commonData.sTress[gg];
            VectorAdaptor normal = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(
                3,&nitscheCommonData.faceNormals[ff](fgg,0)
              )
            );

            VectorAdaptor x = VectorAdaptor(
              3,ublas::shallow_array_adaptor<double>(
                3,&getHoCoordsAtGaussPts()(gg,0)
              )
            );

            tRaction.resize(3,false);
            noalias(tRaction) = prod(normal,stress);

            homoStress[0] += x[0]*tRaction[0]*val;
            homoStress[1] += x[1]*tRaction[1]*val;
            homoStress[2] += x[2]*tRaction[2]*val;
            homoStress[3] += (x[0]*tRaction[1]+x[1]*tRaction[0])*0.5*val;
            homoStress[4] += (x[1]*tRaction[2]+x[2]*tRaction[1])*0.5*val;
            homoStress[5] += (x[0]*tRaction[2]+x[2]*tRaction[0])*0.5*val;
          }
        }

        const int indices_6[6] = {0, 1, 2, 3, 4, 5};
        ierr = VecSetValues(
          stressHomoGhostVector,6,indices_6,&homoStress[0],ADD_VALUES
        ); CHKERRQ(ierr);

        if(gg != (int)getGaussPts().size2()) {
          SETERRQ(PETSC_COMM_SELF,MOFEM_DATA_INCONSISTENCY,"wrong number of gauss pts");
        }


      } catch (const std::exception& ex) {
        ostringstream ss;
        ss << "throw in method: " << ex.what() << endl;
        SETERRQ(PETSC_COMM_SELF,1,ss.str().c_str());
      }

      PetscFunctionReturn(0);
    }

  };


};

#endif //__NITSCHE_PERIODIC_METHOD_HPP__
