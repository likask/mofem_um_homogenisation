!/bin/bash

#1 - input file
#2 - row
#3 - column
#4 - reference stress


awk '
  /ORDER=/ { order=$2 }
  /GAMMA=/ { gamma=$2 }
  /PHI=/ {phi=$2}
  /F=/ {file=$2}
  /partition_problem: rank = 0 FEs row/ { nbdofs=$25 }
  /stress '$2'/ { print order,gamma,phi,file,$'$3',nbdofs }
' $1 \
| sed 's/ORDER=//' | sed 's/GAMMA=//' | sed 's/PHI=//' | sed 's/F=//' \
| sed 's/.h5m//' | sed 's/hole_//' \
| tee tmp1.txt

ORDERLIST=`
  awk '
    {print $1}
  ' tmp1.txt | sort | uniq
`
echo ORDERLIST $ORDERLIST

GAMMALIST=`
  awk '
    {print $2}
  ' tmp1.txt | sort | uniq
`
echo GAMMALIST $GAMMALIST

PHILIST=`
  awk '
    {print $3}
  ' tmp1.txt | sort | uniq
`
echo PHILIST $PHILIST


FILELIST=`
  awk '
    {print $4}
  ' tmp1.txt | sort | uniq
`
echo FILELIST $FILELIST

#h- ceneter
for GAMMA in $GAMMALIST; do
  for PHI in $PHILIST; do
    for ORDER in $ORDERLIST; do
    awk '
    $1~/'$ORDER'/ && $2~/'$GAMMA'/ && ($3=='$PHI') && $4!~/'off'/ { a=$5-'$4'; a=sqrt(a*a)/'$4';  print $6,a }
    ' tmp1.txt | sort -n -k1 | tee tmp2_h_$ORDER\_$GAMMA\_$PHI.txt
    done
  done
done

#h- off ceneter
for GAMMA in $GAMMALIST; do
  for PHI in $PHILIST; do
    for ORDER in $ORDERLIST; do
    awk '
    $1~/'$ORDER'/ && $2~/'$GAMMA'/ && ($3=='$PHI') && $4~/'off'/ { a=$5-'$4'; a=sqrt(a*a)/'$4';  print $6,a }
    ' tmp1.txt | sort -n -k1 | tee tmp2_off_h_$ORDER\_$GAMMA\_$PHI.txt
    done
  done
done

#gnuplot
# gnuplot -persist << EOF
# set log x
# set log y
# set title "Convergence Nitsche's method ({/Symbol \146}=-1 and {/Symbol \170}=10^{-4})"
# set xlabel 'Log(Nb. of dofs)'
# set ylabel 'Log(|{/Symbol \163}-{/Symbol \163}^{ref}|/{/Symbol \163}^{ref})'
# plot [:] \
# './tmp2_h_1_1e-6_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-6} (c)' lw 3 lt 1 pt 1,\
# './tmp2_h_1_1e-5_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-5} (c)' lw 3 lt 1 pt 2,\
# './tmp2_h_1_1e-4_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-4} (c)' lw 3 lt 1 pt 3,\
# './tmp2_off_h_1_1e-6_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-6} (o)' lw 3 lt 2 pt 1,\
# './tmp2_off_h_1_1e-5_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-5} (o)' lw 3 lt 2 pt 2,\
# './tmp2_off_h_1_1e-4_-1.txt' w lp title 'P_1 {/Symbol \147}=10^{-4} (o)' lw 3 lt 2 pt 3,\
# './tmp2_h_2_1e-6_-1.txt' w lp title 'P_2 {/Symbol \147}=10^{-6} (c)' lw 3 lt 3 pt 1,\
# './tmp2_h_2_1e-5_-1.txt' w lp title 'P_2 {/Symbol \147}=10^{-5} (c)' lw 3 lt 3 pt 2,\
# './tmp2_off_h_2_1e-6_-1.txt' w lp title 'P_2 {/Symbol \147}=10^{-6} (c)' lw 3 lt 4 pt 1,\
# './tmp2_off_h_2_1e-5_-1.txt' w lp title 'P_2 {/Symbol \147}=10^{-5} (c)' lw 3 lt 4 pt 2
# set terminal postscript eps enhanced color font 'Helvetica,10'
# set output "my-plot1.ps"
# replot
# EOF
